package com.example.demo

import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde
import org.apache.avro.generic.GenericData
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.*
import org.apache.kafka.streams.state.KeyValueStore
import org.slf4j.LoggerFactory
import java.util.*

class JoinStreamBuilder {
    companion object {
        private val LOG = LoggerFactory.getLogger(JoinStreamBuilder::class.java)
    }

    class KafkaStreamsProperties(bootstrapServers: String, schemaRegistryUrl: String) {
        val properties: Properties = Properties().apply {
            this[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapServers
            this["schema.registry.url"] = schemaRegistryUrl
            this[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "earliest"
            this[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.String()::class.java
            this[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = GenericAvroSerde::class.java
            this[StreamsConfig.STATE_DIR_CONFIG] = createTempDir().apply { deleteOnExit() }.absolutePath
            this[StreamsConfig.STATE_CLEANUP_DELAY_MS_CONFIG] = 1000_000_000
        }
    }

    class GenericAvroSerdeProvider(private val schemaRegistryUrl: String) {
        fun provide(): GenericAvroSerde {
            val serdeConfig = Collections.singletonMap("schema.registry.url", schemaRegistryUrl)
            val valueGenericAvroSerde = GenericAvroSerde()
            valueGenericAvroSerde.configure(serdeConfig, false)
            return valueGenericAvroSerde
        }
    }

    class MainKeySelector : KeyValueMapper<String?, GenericRecord, String> {
        override fun apply(key: String?, mainRecord: GenericRecord): String {
            LOG.trace("applying on main key=$key, streamRecord=$mainRecord")
            return mainRecord[MySampleData::stringField.name].toString()
        }
    }

    class LookupKeySelector : KeyValueMapper<String?, GenericRecord, String> {
        override fun apply(key: String?, lookupRecord: GenericRecord): String {
            LOG.trace("applying on main key=$key, streamRecord=$lookupRecord")
            return lookupRecord[MySampleData::stringField.name].toString()
        }
    }

    class Joiner(private val streamId: Int) : ValueJoiner<GenericRecord, GenericRecord, GenericRecord> {
        override fun apply(main: GenericRecord?, lookup: GenericRecord?): GenericRecord {
            if (main == null) LOG.warn("for streamId: $streamId record from main is null")
            if (lookup == null) LOG.warn("for streamId: $streamId record from lookup is null")

            return GenericData.Record(MySampleData.schema)
                    .apply {
                        put(MySampleData::stringField.name, main?.get(MySampleData::stringField.name))
                        put(MySampleData::booleanField.name, main?.get(MySampleData::booleanField.name))
                        put(MySampleData::intField.name, lookup?.get(MySampleData::intField.name))
                    }
        }
    }

    fun buildJoinStream(kafkaStreamsProperties: KafkaStreamsProperties, streamId: Int, targetTopicName: String, mainTopicName: String, lookupTableTopicName: String, genericAvroSerde: GenericAvroSerde): KafkaStreams {
        val builder = StreamsBuilder()

        val reducedLookupStoreName = "reduced_$lookupTableTopicName$streamId"

        val streamToJoin = builder.stream(mainTopicName, Consumed.with(Serdes.String(), genericAvroSerde))
                .peek { k, v -> LOG.info("$streamId => main: key=$k, value=$v") }
                .selectKey(MainKeySelector())
                .peek { k, v -> LOG.info("$streamId => main: after selectKey: key=$k, value=$v") }

        val lookupTable = builder.stream(lookupTableTopicName, Consumed.with(Serdes.String(), genericAvroSerde))
                .peek { k, v -> LOG.info("$streamId => lookup: key=$k, value=$v") }
                .selectKey(LookupKeySelector())
                .peek { k, v -> LOG.info("$streamId => lookup: after selectKey: key=$k, value=$v") }
                .groupByKey(Grouped.with(Serdes.String(), genericAvroSerde))
                .reduce({ _, new -> new },
                        Materialized.`as`<String, GenericRecord, KeyValueStore<Bytes, ByteArray>>(reducedLookupStoreName).withKeySerde(Serdes.String()).withValueSerde(genericAvroSerde))

        streamToJoin
                .leftJoin(lookupTable, Joiner(streamId), Joined.with(Serdes.String(), genericAvroSerde, genericAvroSerde))
                .peek { k, v -> LOG.info("$streamId => joined: key=$k, value=$v") }
                .to(targetTopicName, Produced.with(Serdes.String(), genericAvroSerde))
        val topology = builder.build()

        return KafkaStreams(topology, kafkaStreamsProperties.properties.apply {
            this[StreamsConfig.APPLICATION_ID_CONFIG] = "stream-join-problem$streamId"
        })
    }

    fun buildJoinStreamWithoutKeySelecting(kafkaStreamsProperties: KafkaStreamsProperties, streamId: Int, targetTopicName: String, mainTopicName: String, lookupTableTopicName: String, genericAvroSerde: GenericAvroSerde): KafkaStreams {
        val builder = StreamsBuilder()

        val reducedLookupStoreName = "reduced_$lookupTableTopicName$streamId"

        val streamToJoin = builder.stream(mainTopicName, Consumed.with(Serdes.String(), genericAvroSerde))
                .peek { k, v -> LOG.info("$streamId => main: key=$k, value=$v") }

        val lookupTable = builder.stream(lookupTableTopicName, Consumed.with(Serdes.String(), genericAvroSerde))
                .peek { k, v -> LOG.info("$streamId => lookup: key=$k, value=$v") }
                .groupByKey(Grouped.with(Serdes.String(), genericAvroSerde))
                .reduce({ _, new -> new },
                        Materialized.`as`<String, GenericRecord, KeyValueStore<Bytes, ByteArray>>(reducedLookupStoreName).withKeySerde(Serdes.String()).withValueSerde(genericAvroSerde))

        streamToJoin
                .leftJoin(lookupTable, Joiner(streamId), Joined.with(Serdes.String(), genericAvroSerde, genericAvroSerde))
                .peek { k, v -> LOG.info("$streamId => joined: key=$k, value=$v") }
                .to(targetTopicName, Produced.with(Serdes.String(), genericAvroSerde))
        val topology = builder.build()

        return KafkaStreams(topology, kafkaStreamsProperties.properties.apply {
            this[StreamsConfig.APPLICATION_ID_CONFIG] = "stream-join-problem$streamId"
        })
    }
}