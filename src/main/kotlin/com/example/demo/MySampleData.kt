package com.example.demo

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecordBuilder

data class MySampleData(val stringField: String? = "",
                        val intField: Int? = 0,
                        val booleanField: Boolean? = null) {
    companion object {
        val schema: Schema?

        init {
            val schemaString = """
                {"type":"record","name":"sampleData","namespace":"namespace","doc":"docs","fields":[{"name":"stringField","type":["null","string"],"doc":"doc","default":null},{"name":"intField","type":["null","int"],"doc":"doc","default":null},{"name":"booleanField","type":["null","boolean"],"doc":"doc","default":null},{"name":"timeFieldMillisPrecision","type":["null",{"type":"int","logicalType":"time-millis"}],"doc":"doc","default":null}]}
            """.trimIndent()

            val parser = Schema.Parser()
            schema = parser.parse(schemaString)

        }
    }

    fun toGenericRecord() = GenericRecordBuilder(schema)
            .set(MySampleData::stringField.name, stringField)
            .set(MySampleData::intField.name, intField)
            .build()
}
