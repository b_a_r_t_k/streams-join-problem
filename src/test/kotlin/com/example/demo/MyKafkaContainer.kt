package com.example.demo

import org.junit.runner.Description
import org.testcontainers.containers.FailureDetectingExternalResource
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.KafkaContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy
import java.time.Duration

class MyKafkaContainer(version: String)
    : KafkaContainer(version), Linkable by AliasAndPort("broker", 9092, "PLAINTEXT://") {

    val zookeeper = AliasAndPort(alias, ZOOKEEPER_PORT, ""/* no schema */)

    init {
        withNetworkAliases(alias)
    }
}

class MyCorePlatformInfrastructure(confluentPlatformVersion: String = Companion.confluentPlatformVersion,
                                   schemaRegistryImage: String = Companion.schemaRegistryImage,
                                   kafkaConfigurationHook: (MyKafkaContainer) -> MyKafkaContainer = ::noOp,
                                   schemaRegistryConfigurationHook: (SchemaRegistryContainer) -> SchemaRegistryContainer = ::noOp) : FailureDetectingExternalResource() {
    val kafka = kafkaConfigurationHook(MyKafkaContainer(version = confluentPlatformVersion))
    val schemaRegistry = schemaRegistryConfigurationHook(SchemaRegistryContainer(image = schemaRegistryImage, network = kafka.network, broker = kafka))
    val network by lazy { kafka.network }

    companion object {
        const val confluentPlatformVersion = "5.0.1"
        const val schemaRegistryImage = "confluentinc/cp-schema-registry:${ContainerDefaults.confluentPlatformVersion}"
    }

    override fun starting(description: Description?) {
        kafka.start()
        schemaRegistry.start()
    }

    override fun finished(description: Description?) {
        kafka.stop()
        schemaRegistry.stop()
    }
}

object ContainerDefaults {
    const val confluentPlatformVersion = "5.0.1"
}


interface Linkable {
    val alias: String
    val port: Int
    fun scheme(): String
    fun internalUrl() = "${scheme()}$alias:$port"
}


class AliasAndPort(override val alias: String, override val port: Int, private val overriddenSchema: String? = null) : Linkable {
    override fun scheme(): String {
        return overriddenSchema ?: "http://"
    }
}

class SchemaRegistryContainer(image: String, network: Network, broker: Linkable)
    : GenericContainer<SchemaRegistryContainer>(image), Linkable by AliasAndPort("schemaRegistry", 8081), Exposable {

    init {
        withNetwork(network)
                .withExposedPorts(port)
                .withNetworkAliases(alias)
                .withEnv(mapOf(
                        "SCHEMA_REGISTRY_HOST_NAME" to alias,
                        "SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS" to broker.internalUrl()
                ))
                .waitingFor(HostPortWaitStrategy().withStartupTimeout(Duration.ofMinutes(2)))
    }
}

interface Exposable {
    fun getFirstMappedPort(): Int
    fun getContainerIpAddress(): String
    fun externalUrl() = "http://${getContainerIpAddress()}:${getFirstMappedPort()}"
}

private fun <T> noOp(arg: T): T = arg
