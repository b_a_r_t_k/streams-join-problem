package com.example.demo

import com.example.demo.JoinStreamBuilder.GenericAvroSerdeProvider
import com.example.demo.JoinStreamBuilder.KafkaStreamsProperties
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.Serdes
import org.awaitility.Awaitility.await
import org.awaitility.Duration.TWO_MINUTES
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Test
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*

class JoinStreamWithKeysTest {
    companion object {
        private val LOG = LoggerFactory.getLogger(JoinStreamWithKeysTest::class.java)

        @ClassRule
        @JvmField
        val env = MyCorePlatformInfrastructure()
    }

    private val genericAvroSerde = GenericAvroSerdeProvider(env.schemaRegistry.externalUrl()).provide()

    private val howManyTimes = 5
    private val howManyRecords = 20

    private val joinStreamBuilderUnderTest = JoinStreamBuilder()

    @Test
    fun testForRecordsWithKeys() {
        val testId = "testWithKeys"
        val sampleLookupData = (1..howManyRecords).map { MySampleData(stringField = it.toString(), intField = it) }
        val sampleJoinData = (1..howManyRecords).map { MySampleData(stringField = it.toString(), booleanField = true) }

        val failuresPerIteration = mutableMapOf<Int, Int>()
        repeat(howManyTimes) { iteration ->
            LOG.info("iteration: $iteration")
            failuresPerIteration[iteration] = 0

            val targetTopicName = "targetTopicName-$testId-$iteration"
            val kafkaStreamsProperties = KafkaStreamsProperties(env.kafka.bootstrapServers, env.schemaRegistry.externalUrl())

            val mainTopicName = "mainTopicName-$testId-$iteration"
            val lookupTableTopicName = "lookupTableTopicName-$testId-$iteration"

            sendTestDataToTopic(kafkaStreamsProperties, sampleLookupData, lookupTableTopicName) { mySampleData -> mySampleData.stringField!! }
            waitForDataInTopic(kafkaStreamsProperties, lookupTableTopicName)
            sendTestDataToTopic(kafkaStreamsProperties, sampleJoinData, mainTopicName) { mySampleData -> mySampleData.stringField!! }
            waitForDataInTopic(kafkaStreamsProperties, mainTopicName)

            val kafkaStreams = joinStreamBuilderUnderTest.buildJoinStreamWithoutKeySelecting(
                    kafkaStreamsProperties = kafkaStreamsProperties,
                    genericAvroSerde = genericAvroSerde,
                    streamId = iteration,
                    targetTopicName = targetTopicName,
                    mainTopicName = mainTopicName,
                    lookupTableTopicName = lookupTableTopicName
            )

            kafkaStreams.start()

            kafkaConsumer(kafkaStreamsProperties, UUID.randomUUID().toString()).use { consumer ->
                consumer.subscribe(listOf(targetTopicName))

                var aggregatedRecords: List<GenericRecord> = listOf()

                await().atMost(TWO_MINUTES)
                        .until {
                            val res = consumer.poll(Duration.ofMillis(2000))

                            aggregatedRecords = aggregatedRecords + res.records(targetTopicName).map {
                                it.value()
                            }

                            try {
                                aggregatedRecords.all { record ->
                                    val stringField = record[MySampleData::stringField.name]
                                    val intField = record[MySampleData::intField.name]
                                    if (stringField == null || intField == null) {
                                        when {
                                            failuresPerIteration[iteration] == null -> failuresPerIteration[iteration] = 1
                                            else -> failuresPerIteration[iteration] = failuresPerIteration[iteration]!! + 1
                                        }

                                        LOG.info("One of them at least is null: stringField: $stringField, intField: $intField")
                                    }
                                    stringField.toString() == intField.toString()
                                }

                            } catch (t: Throwable) {
                                LOG.error("Throwable", t)
                            }
                            aggregatedRecords.size == howManyRecords
                        }
            }
            kafkaStreams.close()
        }

        val totalNumberOfFailedJoins = failuresPerIteration.values.fold(0) { acc, v -> acc + v }
        LOG.info("Number of not properly joined per iteration (iteration number -> number of errors): $failuresPerIteration. Total errors: $totalNumberOfFailedJoins")

        assertEquals(0, totalNumberOfFailedJoins)
    }


    private fun waitForDataInTopic(kafkaStreamsProperties: KafkaStreamsProperties, topicName: String) {
        kafkaConsumer(kafkaStreamsProperties, UUID.randomUUID().toString()).use { consumer ->
            consumer.subscribe(listOf(topicName))

            var aggregatedRecords: List<GenericRecord> = listOf()

            await().atMost(TWO_MINUTES).until {
                val res = consumer.poll(Duration.ofMillis(2000))

                aggregatedRecords = aggregatedRecords + res.records(topicName).map {
                    it.value()
                }
                aggregatedRecords.size == howManyRecords
            }
        }
    }

    private fun kafkaConsumer(kafkaStreamsProperties: KafkaStreamsProperties, groupId: String): KafkaConsumer<String?, GenericRecord> {
        return KafkaConsumer<String?, GenericRecord>(kafkaStreamsProperties.properties.apply {
            this["key.deserializer"] = "org.apache.kafka.common.serialization.StringDeserializer"
            this["value.deserializer"] = "io.confluent.kafka.serializers.KafkaAvroDeserializer"
            this["group.id"] = groupId
        })
    }

    private fun sendTestDataToTopic(kafkaStreamsProperties: KafkaStreamsProperties, testData: List<MySampleData>, topic: String) {
        KafkaProducer(kafkaStreamsProperties.properties, Serdes.String().serializer(), genericAvroSerde.serializer()).use { producer ->
            testData.forEach {
                producer.send(ProducerRecord<String?, GenericRecord>(topic, it.toGenericRecord()))
            }
            producer.flush()
        }
    }

    private fun sendTestDataToTopic(kafkaStreamsProperties: KafkaStreamsProperties, testData: List<MySampleData>, topic: String, keyGenerator: (MySampleData) -> String) {
        KafkaProducer(kafkaStreamsProperties.properties, Serdes.String().serializer(), genericAvroSerde.serializer()).use { producer ->
            testData.forEach {
                producer.send(ProducerRecord<String?, GenericRecord>(topic, keyGenerator(it), it.toGenericRecord()))
            }
            producer.flush()
        }
    }
}

